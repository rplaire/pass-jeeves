﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KeePassLib.Serialization;

namespace KeePass8.Model
{
    public interface IApplicationModel
    {
        Collection<IOConnectionInfo> RecentFiles { get; }

        Task Save(); 
        Task Load();
        void AddRecent(IOConnectionInfo ioc);

    }
}
