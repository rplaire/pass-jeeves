﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using KeePass8.ViewModel;
using KeePass8.ViewModel.Pages;
using KeePassLib;
using KeePassLib.Collections;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;


namespace KeePass8
{
    /// <summary>
    /// A page that displays a grouped collection of password icons.
    /// </summary>
    /// 

    public sealed partial class GroupedPasswordItemGridPage: KeePass8.Common.LayoutAwarePage
    {
      
        public GroupedPasswordItemGridPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected async override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }


        private void itemGridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            ((GroupedPasswordItemVM)DataContext).SelectedItem = ((PasswordItemVM)e.ClickedItem);
            this.Frame.Navigate(typeof(GroupedPasswordItemDetailPage), null);
        }

    }

}

