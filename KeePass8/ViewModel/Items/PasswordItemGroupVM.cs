﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KeePass8.ViewModel;

namespace KeePass8.ViewModel
{
    /// <summary>
    /// A group of password items
    /// </summary>
    public class PasswordItemGroupVM
    {
        private ObservableCollection<PasswordItemVM> m_listPasswordItems = new ObservableCollection<PasswordItemVM>();
        public ObservableCollection<PasswordItemVM> Items
        {
            get { return this.m_listPasswordItems; }
        }

        public IEnumerable<PasswordItemVM> TopItems
        {
            get { return m_listPasswordItems; }
        }

        public string Title { get; set; }
    }

}
