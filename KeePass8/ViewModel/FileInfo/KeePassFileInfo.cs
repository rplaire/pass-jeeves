﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using KeePassLib.Serialization;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;

namespace KeePass8.Model
{
    /// <summary>
    /// Provides information about the file to be opened. Name, thumbnail, size,...
    /// </summary>
    public class KeePassFileInfo : ViewModelBase, IKeePassFileInfo
    {
        private string _fileName = "";
        private ulong _fileSize = 0;
        private BitmapImage _thumbnail = null;
        private DateTimeOffset _dateModified;

        public IOConnectionInfo IOConnection { get; private set; }


        // Observable properties.
        public const string FileNamePropertyName = "FileName";
        public const string FileSizePropertyName = "FileSize";
        public const string ThumbnailPropertyName = "Thumbnail";
        public const string DateModifiedPropertyName = "DateModified";

        public KeePassFileInfo(IOConnectionInfo ioc)
        {
            IOConnection = ioc;
        }

        public async Task UpdateInfo()
        {
            StorageFile file = await Windows.Storage.StorageFile.GetFileFromPathAsync(IOConnection.Path);
            FileName = file.DisplayName;
            StorageItemThumbnail thumb = await file.GetThumbnailAsync(ThumbnailMode.DocumentsView);
            Thumbnail = new BitmapImage();
            Thumbnail.SetSource(thumb);

            BasicProperties properties = await file.GetBasicPropertiesAsync();
            FileSize = properties.Size;
            DateModified = properties.DateModified;
        }



        /// <summary>
        /// Sets and gets the FileName property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string FileName
        {
            get {
                return _fileName; 
            }

            set
            {
                if (_fileName == value)
                {
                    return;
                }

                RaisePropertyChanging(FileNamePropertyName);
                _fileName = value;
                RaisePropertyChanged(FileNamePropertyName);
            }
        }


        /// <summary>
        /// Sets and gets the FileSize property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public ulong FileSize
        {
            get
            {
                return _fileSize;
            }

            set
            {
                if (_fileSize == value)
                {
                    return;
                }

                RaisePropertyChanging(FileSizePropertyName);
                _fileSize = value;
                RaisePropertyChanged(FileSizePropertyName);
            }
        }



        /// <summary>
        /// Sets and gets the Thumbnail property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public BitmapImage Thumbnail
        {
            get
            {
                return _thumbnail;
            }

            set
            {
                if (_thumbnail == value)
                {
                    return;
                }

                RaisePropertyChanging(ThumbnailPropertyName);
                _thumbnail = value;
                RaisePropertyChanged(ThumbnailPropertyName);
            }
        }


        /// <summary>
        /// Sets and gets the DateModified property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public DateTimeOffset DateModified
        {
            get
            {
                return _dateModified;
            }

            set
            {
                if (_dateModified == value)
                {
                    return;
                }

                RaisePropertyChanging(DateModifiedPropertyName);
                _dateModified = value;
                RaisePropertyChanged(DateModifiedPropertyName);
            }
        }
    }
}
