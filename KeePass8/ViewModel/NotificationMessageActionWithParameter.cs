﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;

namespace KeePass8.ViewModel
{

    class NotificationMessageActionWithParameter<TMessageParameter> : NotificationMessageAction<TMessageParameter>
    {
        
        public TMessageParameter CallParameter;
       /// <summary>
       /// A trivial subclass of NotificationMessageAction, to allow sending a call parameter.
       /// </summary>
       /// 
        public NotificationMessageActionWithParameter(TMessageParameter callParameter, Action<TMessageParameter> callback) : base("", callback)
        {
            CallParameter = callParameter;
        }
    }
}
